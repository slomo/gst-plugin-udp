// Copyright (C) 2024 Sebastian Dröge <sebastian@centricular.com>
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0

use gst::subclass::prelude::*;
use gst::{glib, prelude::*};
use gst_base::{
    prelude::*,
    subclass::{base_src::CreateSuccess, prelude::*},
};

use std::{
    collections::VecDeque,
    io,
    net::{IpAddr, Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, SocketAddrV6},
    sync::{Arc, Mutex},
};

use arrayvec::ArrayVec;
use atomic_refcell::AtomicRefCell;
use once_cell::sync::Lazy;

static CAT: Lazy<gst::DebugCategory> = Lazy::new(|| {
    gst::DebugCategory::new(
        "rsudpsrc",
        gst::DebugColorFlags::empty(),
        Some("Rust UDP Source"),
    )
});

const N_MSGS: usize = 32;

const WAKER_TOKEN: mio::Token = mio::Token(0);
const SOCKET_TOKEN: mio::Token = mio::Token(1);

#[derive(Debug, Clone)]
struct Settings {
    address: IpAddr,
    port: u16,
    buffer_size: u32,
    max_packet_size: u32,
    caps: Option<gst::Caps>,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            address: IpAddr::V4(Ipv4Addr::UNSPECIFIED),
            port: 5000,
            buffer_size: 0,
            max_packet_size: 1500,
            caps: None,
        }
    }
}

struct State {
    poll: Option<mio::Poll>,
    events: mio::Events,
    socket: Option<mio::net::UdpSocket>,
    waker: Option<Arc<mio::Waker>>,
    buffer_pool: Option<gst::BufferPool>,
    buffers_cache: VecDeque<gst::MappedBuffer<gst::buffer::Writable>>,
}

impl Default for State {
    fn default() -> Self {
        State {
            poll: None,
            events: mio::Events::with_capacity(2),
            socket: None,
            waker: None,
            buffer_pool: None,
            buffers_cache: VecDeque::with_capacity(N_MSGS),
        }
    }
}

#[derive(Default)]
pub struct UdpSrc {
    settings: Mutex<Settings>,
    state: AtomicRefCell<State>,
    waker: Mutex<Option<Arc<mio::Waker>>>,
}

#[glib::object_subclass]
impl ObjectSubclass for UdpSrc {
    const NAME: &'static str = "RsUdpSrc";
    type Type = super::UdpSrc;
    type ParentType = gst_base::PushSrc;
}

impl ObjectImpl for UdpSrc {
    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
            vec![
                glib::ParamSpecString::builder("address")
                    .nick("Address")
                    .blurb("IP Address to bind to")
                    .default_value("0.0.0.0")
                    .mutable_ready()
                    .build(),
                glib::ParamSpecUInt::builder("port")
                    .nick("Port")
                    .blurb("Port to bind to")
                    .default_value(Settings::default().port as u32)
                    .mutable_ready()
                    .build(),
                glib::ParamSpecUInt::builder("buffer-size")
                    .nick("Buffer Size")
                    .blurb("Socket receive buffer size")
                    .default_value(Settings::default().buffer_size)
                    .mutable_ready()
                    .build(),
                glib::ParamSpecUInt::builder("max-packet-size")
                    .nick("Maximum Packet Size")
                    .blurb("Maximum received packet size")
                    .default_value(Settings::default().max_packet_size)
                    .mutable_ready()
                    .build(),
                glib::ParamSpecBoxed::builder::<gst::Caps>("caps")
                    .nick("Caps")
                    .blurb("Caps of the received stream")
                    .mutable_ready()
                    .build(),
            ]
        });

        PROPERTIES.as_ref()
    }

    fn constructed(&self) {
        self.parent_constructed();

        let obj = self.obj();
        obj.set_live(true);
        obj.set_format(gst::Format::Time);
        obj.set_element_flags(gst::ElementFlags::REQUIRE_CLOCK);
    }

    fn set_property(&self, _id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
        match pspec.name() {
            "address" => {
                let mut settings = self.settings.lock().unwrap();
                let address = value.get::<Option<&str>>().expect("type checked upstream");

                let address = match address {
                    Some(address) => address,
                    None => {
                        gst::info!(
                            CAT,
                            imp = self,
                            "Changing address from {} to 0.0.0.0",
                            settings.address,
                        );
                        settings.address = IpAddr::V4(Ipv4Addr::UNSPECIFIED);
                        return;
                    }
                };

                let address = match address.parse::<IpAddr>() {
                    Ok(address) => address,
                    Err(err) => {
                        gst::error!(CAT, imp = self, "Failed parsing address {address}: {err}",);
                        return;
                    }
                };

                gst::info!(
                    CAT,
                    imp = self,
                    "Changing address from {} to {address}",
                    settings.address,
                );
                settings.address = address;
            }
            "port" => {
                let mut settings = self.settings.lock().unwrap();
                let port = value.get::<u32>().expect("type checked upstream");
                gst::info!(
                    CAT,
                    imp = self,
                    "Changing port from {} to {port}",
                    settings.port,
                );
                settings.port = u16::try_from(port).unwrap_or(0);
            }
            "buffer-size" => {
                let mut settings = self.settings.lock().unwrap();
                let buffer_size = value.get().expect("type checked upstream");
                gst::info!(
                    CAT,
                    imp = self,
                    "Changing buffer-size from {} to {buffer_size}",
                    settings.buffer_size,
                );
                settings.buffer_size = buffer_size;
            }
            "max-packet-size" => {
                let mut settings = self.settings.lock().unwrap();
                let max_packet_size = value.get().expect("type checked upstream");
                gst::info!(
                    CAT,
                    imp = self,
                    "Changing max-packet-size from {} to {max_packet_size}",
                    settings.max_packet_size,
                );
                settings.max_packet_size = max_packet_size;
            }
            "caps" => {
                let mut settings = self.settings.lock().unwrap();
                let caps = value.get().expect("type checked upstream");
                gst::info!(
                    CAT,
                    imp = self,
                    "Changing caps from {:?} to {caps:?}",
                    settings.caps,
                );
                settings.caps = caps;
            }
            _ => unimplemented!(),
        }
    }

    fn property(&self, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        match pspec.name() {
            "address" => {
                let settings = self.settings.lock().unwrap();
                settings.address.to_string().to_value()
            }
            "port" => {
                let settings = self.settings.lock().unwrap();
                (settings.port as u32).to_value()
            }
            "buffer-size" => {
                let settings = self.settings.lock().unwrap();
                settings.buffer_size.to_value()
            }
            "max-packet-size" => {
                let settings = self.settings.lock().unwrap();
                settings.max_packet_size.to_value()
            }
            "caps" => {
                let settings = self.settings.lock().unwrap();
                settings.caps.to_value()
            }
            _ => unimplemented!(),
        }
    }
}

impl GstObjectImpl for UdpSrc {}

impl ElementImpl for UdpSrc {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: Lazy<gst::subclass::ElementMetadata> = Lazy::new(|| {
            gst::subclass::ElementMetadata::new(
                "UDP Source",
                "Source/Network",
                "Reads an UDP stream",
                "Sebastian Dröge <sebastian@centricular.com>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }

    fn pad_templates() -> &'static [gst::PadTemplate] {
        static PAD_TEMPLATES: Lazy<Vec<gst::PadTemplate>> = Lazy::new(|| {
            let caps = gst::Caps::new_any();
            let src_pad_template = gst::PadTemplate::new(
                "src",
                gst::PadDirection::Src,
                gst::PadPresence::Always,
                &caps,
            )
            .unwrap();

            vec![src_pad_template]
        });

        PAD_TEMPLATES.as_ref()
    }
}

impl BaseSrcImpl for UdpSrc {
    fn start(&self) -> Result<(), gst::ErrorMessage> {
        let settings = self.settings.lock().unwrap();

        let mut state = self.state.borrow_mut();

        let poll = mio::Poll::new().map_err(|err| {
            gst::error_msg!(gst::ResourceError::OpenRead, ["Failed create poll: {err}"])
        })?;
        let waker = Arc::new(
            mio::Waker::new(poll.registry(), WAKER_TOKEN).map_err(|err| {
                gst::error_msg!(gst::ResourceError::OpenRead, ["Failed create waker: {err}"])
            })?,
        );

        {
            let mut waker_storage = self.waker.lock().unwrap();
            *waker_storage = Some(waker.clone());
        }

        let saddr = SocketAddr::new(settings.address, settings.port);

        let bind_saddr = if settings.address.is_multicast() {
            if settings.address.is_ipv4() {
                SocketAddr::from(SocketAddrV4::new(Ipv4Addr::UNSPECIFIED, settings.port))
            } else {
                SocketAddr::from(SocketAddrV6::new(
                    Ipv6Addr::UNSPECIFIED,
                    settings.port,
                    0,
                    0,
                ))
            }
        } else {
            saddr
        };

        let mut socket = mio::net::UdpSocket::bind(bind_saddr).map_err(|err| {
            gst::error_msg!(
                gst::ResourceError::OpenRead,
                ["Failed to bind to {bind_saddr}: {err}"]
            )
        })?;

        if settings.buffer_size > 0 {
            unsafe {
                use std::{mem, os::fd::AsRawFd};

                let raw_fd = socket.as_raw_fd();
                if libc::setsockopt(
                    raw_fd,
                    libc::SOL_SOCKET,
                    libc::SO_RCVBUF,
                    &settings.buffer_size as *const u32 as *const _,
                    mem::size_of_val(&settings.buffer_size) as u32,
                ) != 0
                {
                    let err = io::Error::last_os_error();
                    return Err(gst::error_msg!(
                        gst::ResourceError::OpenRead,
                        [
                            "Failed to set buffer-size to {}: {err}",
                            settings.buffer_size,
                        ]
                    ));
                }
            }
        }

        #[allow(unused_mut)]
        let mut gro_segments = 1;
        #[cfg(target_os = "linux")]
        {
            unsafe {
                use std::{mem, os::fd::AsRawFd};

                let raw_fd = socket.as_raw_fd();
                let enabled = 1i32;
                if libc::setsockopt(
                    raw_fd,
                    libc::SOL_UDP,
                    libc::UDP_GRO,
                    &enabled as *const i32 as *const _,
                    mem::size_of_val(&enabled) as u32,
                ) != 0
                {
                    let err = io::Error::last_os_error();
                    gst::warning!(CAT, imp = self, "Can't enable GRO: {err}");
                } else {
                    // Maximum number of GRO segments per message that Linux would output.
                    // We need to have enough space for that many packets per message.
                    gro_segments = 64;
                }
            }
        }

        poll.registry()
            .register(&mut socket, SOCKET_TOKEN, mio::Interest::READABLE)
            .map_err(|err| {
                gst::error_msg!(
                    gst::ResourceError::OpenRead,
                    ["Failed to register socket: {err}"]
                )
            })?;

        if settings.address.is_multicast() {
            match settings.address {
                IpAddr::V4(ref addr) => socket.join_multicast_v4(addr, &Ipv4Addr::UNSPECIFIED),
                IpAddr::V6(ref addr) => socket.join_multicast_v6(addr, 0),
            }
            .map_err(|err| {
                gst::error_msg!(
                    gst::ResourceError::OpenRead,
                    [
                        "Failed to join multicast group to {}: {err}",
                        settings.address,
                    ]
                )
            })?;
        }

        let buffer_pool = gst::BufferPool::new();
        let mut config = buffer_pool.config();
        config.set_params(None, settings.max_packet_size * gro_segments, 100, 0);
        buffer_pool.set_config(config).map_err(|err| {
            gst::error_msg!(
                gst::ResourceError::OpenRead,
                ["Failed to configure buffer pool: {err}"]
            )
        })?;
        buffer_pool.set_active(true).map_err(|err| {
            gst::error_msg!(
                gst::ResourceError::OpenRead,
                ["Failed to start buffer pool: {err}"]
            )
        })?;

        state.poll = Some(poll);
        state.waker = Some(waker);
        state.socket = Some(socket);
        state.buffer_pool = Some(buffer_pool);

        if let Some(ref caps) = settings.caps {
            let _ = self.obj().set_caps(caps);
        }

        gst::info!(CAT, imp = self, "Started");

        Ok(())
    }

    fn stop(&self) -> Result<(), gst::ErrorMessage> {
        *self.state.borrow_mut() = State::default();
        *self.waker.lock().unwrap() = None;

        gst::info!(CAT, imp = self, "Stopped");

        Ok(())
    }

    fn is_seekable(&self) -> bool {
        false
    }

    fn unlock(&self) -> Result<(), gst::ErrorMessage> {
        gst::debug!(CAT, imp = self, "Unlocking");
        if let Some(waker) = self.waker.lock().unwrap().take() {
            let _ = waker.wake();
        }
        gst::debug!(CAT, imp = self, "Unlocked");

        Ok(())
    }

    fn unlock_stop(&self) -> Result<(), gst::ErrorMessage> {
        gst::debug!(CAT, imp = self, "Stopping unlocking");
        let state = self.state.borrow();
        if let Some(ref waker) = state.waker {
            let mut waker_storage = self.waker.lock().unwrap();
            *waker_storage = Some(waker.clone());
        }
        gst::debug!(CAT, imp = self, "Stopped unlocking");

        Ok(())
    }
}

impl PushSrcImpl for UdpSrc {
    fn create(
        &self,
        _buffer: Option<&mut gst::BufferRef>,
    ) -> Result<CreateSuccess, gst::FlowError> {
        self.recv()
    }
}

impl UdpSrc {
    #[cfg(target_os = "linux")]
    fn recv(&self) -> Result<CreateSuccess, gst::FlowError> {
        use std::cmp;

        let mut state = self.state.borrow_mut();
        let State {
            ref mut poll,
            ref mut events,
            ref mut socket,
            ref waker,
            ref buffer_pool,
            ref mut buffers_cache,
        } = &mut *state;

        {
            let mut waker_storage = self.waker.lock().unwrap();
            *waker_storage = waker.clone();
        }

        let poll = poll.as_mut().unwrap();
        let socket = socket.as_mut().unwrap();
        let buffer_pool = buffer_pool.as_ref().unwrap();

        let Some(clock) = self.obj().clock() else {
            gst::error!(CAT, imp = self, "Need a clock");
            return Err(gst::FlowError::Error);
        };
        let Some(base_time) = self.obj().base_time() else {
            gst::error!(CAT, imp = self, "Need a base time");
            return Err(gst::FlowError::Error);
        };

        'outer_loop: loop {
            // First read all packets that are currently available

            loop {
                let mut buffers = ArrayVec::<
                    (
                        gst::MappedBuffer<gst::buffer::Writable>,
                        usize,
                        Option<SocketAddr>,
                        usize,
                    ),
                    N_MSGS,
                >::new();
                for _ in 0..buffers.capacity() {
                    let Some(buffer) = buffers_cache.pop_back().or_else(|| {
                        buffer_pool
                            .acquire_buffer(None)
                            .ok()
                            .map(|b| b.into_mapped_buffer_writable().unwrap())
                    }) else {
                        return Err(gst::FlowError::Error);
                    };
                    buffers.push((buffer, 0, None, 0));
                }
                let mut buffers = buffers.into_inner().unwrap();

                match self.recvmmsg(socket, &mut buffers) {
                    Ok(()) => {
                        let now = clock.time().unwrap();
                        let running_time = now.saturating_sub(base_time);

                        let num_packets = buffers
                            .iter()
                            .map(|(_, read, _, stride)| {
                                if *read != 0 {
                                    (read + stride - 1) / stride
                                } else {
                                    0
                                }
                            })
                            .sum::<usize>();

                        if num_packets == 0 {
                            for (buffer, _, _, _) in buffers {
                                buffers_cache.push_back(buffer);
                            }
                            continue;
                        }

                        gst::trace!(
                            CAT,
                            imp = self,
                            "Read {num_packets} packets for running time {running_time}",
                        );

                        if num_packets == 1 {
                            let mut new_buffer = None;
                            for (buffer, read, peer, stride) in buffers {
                                if read != 0 {
                                    let peer = peer.unwrap();

                                    gst::trace!(
                                        CAT,
                                        imp = self,
                                        "Read {read} bytes with stride {stride} from {peer}",
                                    );

                                    assert!(new_buffer.is_none());
                                    let mut buffer = buffer.into_buffer();
                                    let buffer_ref = buffer.get_mut().unwrap();
                                    buffer_ref.set_size(read);
                                    buffer_ref.set_dts(running_time);

                                    new_buffer = Some(buffer);
                                } else {
                                    buffers_cache.push_back(buffer);
                                }
                            }

                            gst::trace!(CAT, imp = self, "Outputting single buffer");
                            return Ok(CreateSuccess::NewBuffer(new_buffer.unwrap()));
                        } else {
                            let mut buffer_list = gst::BufferList::new_sized(num_packets);
                            let buffer_list_ref = buffer_list.get_mut().unwrap();

                            for (buffer, read, peer, stride) in buffers {
                                if read != 0 {
                                    let peer = peer.unwrap();

                                    gst::trace!(
                                        CAT,
                                        imp = self,
                                        "Read {read} bytes with stride {stride} from {peer}",
                                    );

                                    let mut buffer = buffer.into_buffer();
                                    let num_sub_buffers = (read + stride - 1) / stride;

                                    if num_sub_buffers == 1 {
                                        let buffer_ref = buffer.get_mut().unwrap();
                                        buffer_ref.set_size(read);
                                        buffer_ref.set_dts(running_time);
                                        buffer_list_ref.add(buffer);
                                    } else {
                                        for idx in 0..num_sub_buffers {
                                            let mut sub_buffer = buffer
                                                .copy_region(
                                                    gst::BufferCopyFlags::MEMORY,
                                                    (idx * stride)
                                                        ..cmp::min((idx + 1) * stride, read),
                                                )
                                                .unwrap();

                                            let sub_buffer_ref = sub_buffer.get_mut().unwrap();
                                            sub_buffer_ref.set_dts(running_time);
                                            buffer_list_ref.add(sub_buffer);
                                        }
                                    }
                                } else {
                                    buffers_cache.push_back(buffer);
                                }
                            }

                            gst::trace!(
                                CAT,
                                imp = self,
                                "Outputting buffer list with {} buffers",
                                buffer_list.len()
                            );

                            return Ok(CreateSuccess::NewBufferList(buffer_list));
                        }
                    }
                    Err(err) if err.kind() == io::ErrorKind::UnexpectedEof => {
                        gst::debug!(CAT, imp = self, "Socket closed");
                        return Err(gst::FlowError::Eos);
                    }
                    Err(err) if err.kind() == io::ErrorKind::Interrupted => {
                        for (buffer, _, _, _) in buffers {
                            buffers_cache.push_back(buffer);
                        }
                        continue;
                    }
                    Err(err) if err.kind() == io::ErrorKind::WouldBlock => {
                        for (buffer, _, _, _) in buffers {
                            buffers_cache.push_back(buffer);
                        }
                        break;
                    }
                    Err(err) => {
                        gst::error!(CAT, imp = self, "Read error: {err}");
                        return Err(gst::FlowError::Error);
                    }
                }
            }

            // Otherwise wait for packets to be available

            gst::trace!(CAT, imp = self, "Polling");
            if let Err(err) = poll.poll(events, None) {
                gst::error!(CAT, imp = self, "Poll error: {err}");
                return Err(gst::FlowError::Error);
            };

            for event in events.iter() {
                if event.token() == WAKER_TOKEN {
                    let waker_storage = self.waker.lock().unwrap();
                    if waker_storage.is_none() {
                        gst::debug!(CAT, imp = self, "Flushing");
                        return Err(gst::FlowError::Flushing);
                    }
                } else if event.token() == SOCKET_TOKEN {
                    if event.is_readable() {
                        gst::trace!(CAT, imp = self, "Socket readable again");
                        continue 'outer_loop;
                    } else if event.is_read_closed() || event.is_write_closed() {
                        gst::debug!(CAT, imp = self, "Socket closed");
                        return Err(gst::FlowError::Eos);
                    } else if event.is_error() {
                        gst::error!(CAT, imp = self, "Socket error");
                        return Err(gst::FlowError::Error);
                    }
                } else {
                    // Spurious wakeup
                }
            }
        }
    }

    #[cfg(target_os = "linux")]
    fn recvmmsg(
        &self,
        socket: &mio::net::UdpSocket,
        buffers: &mut [(
            gst::MappedBuffer<gst::buffer::Writable>,
            usize,
            Option<SocketAddr>,
            usize,
        ); N_MSGS],
    ) -> Result<(), io::Error> {
        use std::{mem, ptr};

        #[repr(C)]
        #[derive(Clone, Copy)]
        union Ctrl {
            _buf: [u8; unsafe { libc::CMSG_SPACE(mem::size_of::<i32>() as u32) } as usize],
            _align: libc::cmsghdr,
        }

        unsafe {
            let mut names = [mem::MaybeUninit::<libc::sockaddr_storage>::uninit(); N_MSGS];
            let mut ctrls = [mem::MaybeUninit::<Ctrl>::uninit(); N_MSGS];
            let mut iovecs = std::array::from_fn::<libc::iovec, N_MSGS, _>(|i| libc::iovec {
                iov_base: buffers[i].0.as_mut_slice().as_mut_ptr() as *mut _,
                iov_len: buffers[i].0.len(),
            });
            let mut hdrs = [mem::zeroed::<libc::mmsghdr>(); N_MSGS];

            for (i, hdr) in hdrs.iter_mut().enumerate() {
                hdr.msg_hdr.msg_iov = &mut iovecs[i];
                hdr.msg_hdr.msg_iovlen = 1;

                hdr.msg_hdr.msg_name = names[i].as_mut_ptr() as *mut _;
                hdr.msg_hdr.msg_namelen = mem::size_of_val(&names[i]) as u32;

                hdr.msg_hdr.msg_control = ctrls[i].as_mut_ptr() as *mut _;
                hdr.msg_hdr.msg_controllen = mem::size_of_val(&ctrls[i]);

                hdr.msg_hdr.msg_flags = 0;
            }

            let n_msgs = socket.try_io(|| {
                use std::os::fd::AsRawFd;

                let n_msgs = libc::recvmmsg(
                    socket.as_raw_fd(),
                    hdrs.as_mut_ptr(),
                    hdrs.len() as u32,
                    0,
                    ptr::null_mut(),
                );
                if n_msgs == -1 {
                    Err(io::Error::last_os_error())
                } else if n_msgs == 0 {
                    Err(io::Error::from(io::ErrorKind::Interrupted))
                } else {
                    Ok(n_msgs as usize)
                }
            })?;

            let mut out_idx = 0;

            for (i, hdr) in hdrs.iter().enumerate().take(n_msgs) {
                if hdr.msg_hdr.msg_flags & libc::MSG_TRUNC != 0 {
                    gst::warning!(CAT, imp = self, "Message truncated");
                    continue;
                }
                assert!(hdr.msg_hdr.msg_flags & libc::MSG_CTRUNC == 0);

                let read = hdr.msg_len as usize;
                let cmsg = libc::CMSG_FIRSTHDR(&hdr.msg_hdr);

                let stride = if cmsg.is_null() {
                    read
                } else {
                    let cmsg = &*cmsg;

                    assert_eq!(cmsg.cmsg_level, libc::SOL_UDP);
                    assert_eq!(cmsg.cmsg_type, libc::UDP_GRO);

                    ptr::read(libc::CMSG_DATA(cmsg as *const _) as *const i32) as usize
                };

                let name = names[i].assume_init();

                let addr = match name.ss_family as i32 {
                    libc::AF_INET => {
                        let addr: &libc::sockaddr_in =
                            &*(&name as *const _ as *const libc::sockaddr_in);
                        SocketAddr::V4(SocketAddrV4::new(
                            Ipv4Addr::from(addr.sin_addr.s_addr.to_ne_bytes()),
                            u16::from_be(addr.sin_port),
                        ))
                    }
                    libc::AF_INET6 => {
                        let addr: &libc::sockaddr_in6 =
                            &*(&name as *const _ as *const libc::sockaddr_in6);
                        SocketAddr::V6(SocketAddrV6::new(
                            Ipv6Addr::from(addr.sin6_addr.s6_addr),
                            u16::from_be(addr.sin6_port),
                            addr.sin6_flowinfo,
                            addr.sin6_scope_id,
                        ))
                    }
                    _ => unreachable!("name.ss_family == {}", name.ss_family),
                };

                buffers[out_idx].1 = read;
                buffers[out_idx].2 = Some(addr);
                buffers[out_idx].3 = stride;

                out_idx += 1;
            }

            for buffer in buffers.iter_mut().skip(out_idx) {
                buffer.1 = 0;
                buffer.2 = None;
                buffer.3 = 0;
            }

            if out_idx == 0 {
                Err(io::Error::from(io::ErrorKind::Interrupted))
            } else {
                Ok(())
            }
        }
    }

    #[cfg(not(target_os = "linux"))]
    fn recv(&self) -> Result<CreateSuccess, gst::FlowError> {
        let mut state = self.state.borrow_mut();
        let State {
            ref mut poll,
            ref mut events,
            ref mut socket,
            ref waker,
            ref buffer_pool,
            ref mut buffers_cache,
        } = &mut *state;

        {
            let mut waker_storage = self.waker.lock().unwrap();
            *waker_storage = waker.clone();
        }

        let poll = poll.as_mut().unwrap();
        let socket = socket.as_mut().unwrap();
        let buffer_pool = buffer_pool.as_ref().unwrap();

        let Some(clock) = self.obj().clock() else {
            gst::error!(CAT, imp = self, "Need a clock");
            return Err(gst::FlowError::Error);
        };
        let Some(base_time) = self.obj().base_time() else {
            gst::error!(CAT, imp = self, "Need a base time");
            return Err(gst::FlowError::Error);
        };

        let mut buffer_list = gst::BufferList::new();
        let buffer_list_ref = buffer_list.get_mut().unwrap();

        'outer_loop: loop {
            // First read all packets that are currently available

            loop {
                let Some(mut buffer) = buffers_cache.pop_back().or_else(|| {
                    buffer_pool
                        .acquire_buffer(None)
                        .ok()
                        .map(|b| b.into_mapped_buffer_writable().unwrap())
                }) else {
                    return Err(gst::FlowError::Error);
                };

                match socket.recv_from(&mut buffer) {
                    Ok((read, peer)) => {
                        let now = clock.time().unwrap();
                        let running_time = now.saturating_sub(base_time);
                        gst::trace!(
                            CAT,
                            imp = self,
                            "Read {read} bytes from {peer} for running time {running_time}",
                        );

                        let mut buffer = buffer.into_buffer();
                        let buffer_ref = buffer.get_mut().unwrap();

                        buffer_ref.set_size(read);
                        buffer_ref.set_dts(running_time);
                        buffer_list_ref.add(buffer);

                        continue;
                    }
                    Err(err) if err.kind() == io::ErrorKind::UnexpectedEof => {
                        gst::debug!(CAT, imp = self, "Socket closed");
                        return Err(gst::FlowError::Eos);
                    }
                    Err(err) if err.kind() == io::ErrorKind::Interrupted => {
                        buffers_cache.push_back(buffer);
                        continue;
                    }
                    Err(err) if err.kind() == io::ErrorKind::WouldBlock => {
                        buffers_cache.push_back(buffer);
                        break;
                    }
                    Err(err) => {
                        gst::error!(CAT, imp = self, "Read error: {err}");
                        return Err(gst::FlowError::Error);
                    }
                }
            }

            if !buffer_list_ref.is_empty() {
                break 'outer_loop;
            }

            // Otherwise wait for packets to be available
            gst::trace!(CAT, imp = self, "Polling");
            if let Err(err) = poll.poll(events, None) {
                gst::error!(CAT, imp = self, "Poll error: {err}");
                return Err(gst::FlowError::Error);
            };

            for event in events.iter() {
                if event.token() == WAKER_TOKEN {
                    let waker_storage = self.waker.lock().unwrap();
                    if waker_storage.is_none() {
                        gst::debug!(CAT, imp = self, "Flushing");
                        return Err(gst::FlowError::Flushing);
                    }
                } else if event.token() == SOCKET_TOKEN {
                    if event.is_readable() {
                        gst::trace!(CAT, imp = self, "Socket readable again");
                        continue 'outer_loop;
                    } else if event.is_read_closed() || event.is_write_closed() {
                        gst::debug!(CAT, imp = self, "Socket closed");
                        return Err(gst::FlowError::Eos);
                    } else if event.is_error() {
                        gst::error!(CAT, imp = self, "Socket error");
                        return Err(gst::FlowError::Error);
                    }
                } else {
                    // Spurious wakeup
                }
            }
        }

        gst::trace!(
            CAT,
            imp = self,
            "Outputting buffer list with {} buffers",
            buffer_list.len()
        );

        Ok(CreateSuccess::NewBufferList(buffer_list))
    }
}
